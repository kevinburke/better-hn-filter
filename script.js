var shouldHideStory = function(votes, comments) {
    return !(votes > 400 || (votes > 10 && (votes / comments) >= 4));
};

var parseScoreFromText = function(text, pointType) {
    return parseInt(text.replace(" " + pointType, ""), 10);
};

$(function() {
    $(".subtext span").each(function() {
        var votes = parseScoreFromText($(this).text(), /points?/);
        var commentText = $(this).parent().children("a").last().text();
        var comments = parseScoreFromText(commentText, /comments?/);
        if (shouldHideStory(votes, comments)) {
            var tr = $(this).parent().parent();
            // hide the story link
            tr.prev().hide();
            // hide the 5px spacing
            tr.next().hide();
            // hide the meta data
            tr.hide();
        }
        return;
    });
});
