# Better Hacker News is a small bit of Javascript that hides any article with too many comments. 
I wrote it when I noticed that most of the articles I wanted to read most - the
technical articles - had many upvotes and few comments, and most of the
articles I shouldn't have bothered to read - the social psychology posts, news
stories or similar - had many comments and few upvotes.

### The specific formula

You will see articles which meet these criteria:

* Any article with over 400 votes
* Articles between 10 and 400 votes, with more than 4x as many votes as
  comments.
* Job listings.

Or in code:
    
    var shouldHideStory = function(votes, comments) {
        return !(votes > 400 || (votes > 10 && (votes / comments) >= 4));
    };
    

And that's it!

### Installation

[Click here to download the file as a Chrome extension](https://bitbucket.org/kevinburke/better-hn-filter/raw/master/better-hn.crx)

### Feedback

Email me at [kevin@twilio.com](mailto:kevin@twilio.com).
